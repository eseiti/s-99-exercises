object P03 {
  def main(args: Array[String]) = {
    println(nth(2, List(1, 1, 2, 3, 5, 8)))
  }
  def nth[A](k:Int, list: List[Int]): Int = (k,list) match
    case (0, el::tail) => el
    case (_, Nil) => throw new NoSuchElementException
    case (c, elements) => nth(c-1, elements.tail)
}
