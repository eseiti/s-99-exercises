import java.util.Random
object P22 {
  def main(args: Array[String]) = println(range(4, 9))

  def range(value: Int, value1: Int) = for i <- value to value1
                                             yield i

}
