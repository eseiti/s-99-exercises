object P12 {
  def main(args: Array[String]) = println(decode(List((4, 'a'), (1, 'b'), (2, 'c'), (2, 'a'), (1, 'd'), (4, 'e'))))

  def decode(value: List[(Int,Char)]): List[Char] = value.flatMap(e => List.fill(e._1)(e._2))
}
