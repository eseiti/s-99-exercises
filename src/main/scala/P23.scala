
object P23 {
  def main(args: Array[String]) = println(randomSelect(3, List("a", "b", "c", "d", "f", "g", "h")))

  def randomSelect[A](n: Int, ls: List[String]): List[String] =
    if (n <= 0) Nil
    else {
      val (rest, e) = P20.removeAt((new util.Random).nextInt(ls.length), ls)
      e :: randomSelect(n - 1, rest)
    }

}
