object P11 {
  def main(args: Array[String]) = println(encodeModified(List('a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e')))

  def encodeModified(value: List[Char]) :List[Any] = P09.pack(value).map(e=> {
    if e.length>1 then (e.length, e)
    else e.head
  })
}
