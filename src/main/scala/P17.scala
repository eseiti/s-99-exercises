
object P17 {
  def main(args: Array[String]) = println(split(3, List('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k')))

  def split(value: Int, value1: List[Char]): (List[Char], List[Char]) =
    def _split(value:Int, acc: List[Char], value1: List[Char]):(List[Char], List[Char]) = (value,value1) match
      case (0, tail) => (acc.reverse, tail)
      case (_, Nil) => (acc.reverse, Nil)
      case(_, el::tail) => _split(value-1, el::acc,tail)

    _split(value, Nil, value1)


}
