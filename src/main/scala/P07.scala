import P06.isPalindrome

object P07 {
  def main(args: Array[String]) = print(flatten(List(List(1, 1), 2, List(3, List(5, 8)))))

  def flatten(l: List[Any]): List[Any] = l match {
    case Nil => Nil
    case (h:List[_])::tail => flatten(h):::flatten(tail)
    case h::tail => h::flatten(tail)
  }
//

}
