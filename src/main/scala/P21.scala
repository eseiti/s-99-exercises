object P21 {
  def main(args: Array[String]) = println(insertAt("new", 1, List("a", "b", "c", "d")))

  def insertAt(s: String, index: Int, ls: List[String]): List[String] =
    def _insertAt(s: String,index:Int, ls: List[String], app: List[String]): List[String] =
      if index == 0 then app.reverse :::s ::ls
      else _insertAt(s, index-1, ls.tail, ls.head::app)

    _insertAt(s,index, ls, Nil)
}
