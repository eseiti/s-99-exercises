object P25 {
  def main(args: Array[String]) = println(randomPermute(List("a", "b", "c", "d", "e", "f")))

  def randomPermute(ls: List[String]): List[String] =  P23.randomSelect(ls.length, ls)

}
