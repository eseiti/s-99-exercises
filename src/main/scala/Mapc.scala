object Mapc {
  def main(args: Array[String]) = println(map(List(1,2,3,4), (a:Int) => a*a))

  def map[A,B](ls: List[A], f: A=>B): List[B] = ls match
    case Nil =>Nil
    case el:: tail => f(el)::map(tail, f)
}
