object P02 {
  def penultimate[A](list: List[A]): A = list match {
    case Nil => throw new NoSuchElementException
    case h :: _ :: Nil => h
    case _ :: tail => penultimate(tail)
  }
}
