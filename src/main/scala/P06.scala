object P06 {
  def main(args: Array[String]) = print(isPalindrome(List(1, 2, 3, 2, 2)))
  def isPalindrome(lst:List[Int]): Boolean = lst == P05.reverse(lst)
}
