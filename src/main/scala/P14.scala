object P14 {
  def main(args: Array[String]) = println(duplicate(List(
    'a', 'b', 'c', 'c', 'd')))

  //def duplicate(value:List[Char]):List[Char] = value.flatMap(e=> List(e,e))

  def duplicate(value: List[Char]): List[Char] = value match {
    case Nil => Nil
    case el::tail => el+:el+: duplicate(tail)
  }
}
