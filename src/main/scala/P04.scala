object P04 {
  def main(args: Array[String]) = print(length(List(1, 1, 2, 3, 5, 8)))

  def length[A](lst:List[A]):Int =
    def length_priv[A](lengthVal:Int, lst:List[A]):Int = lst match
      case _::tail => length_priv(lengthVal+1, tail)
      case Nil => lengthVal

    length_priv(0, lst)

}
