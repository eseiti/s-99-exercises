object P16 {
  def main(args: Array[String]) = println(drop(3, List('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k')))

  // not tail recursive
  def drop(n:Int, ls: List[Char]): List[Char] =
    def _drop(index:Int ,n:Int, ls: List[Char]):List[Char] = ls match
      case Nil => Nil
      case el::tail => if index % n != 0 then el::_drop(index+1, n, tail)
                       else _drop(index+1, n, tail)

    _drop(1, n, ls)
}
