
object P26 {

  def main(args: Array[String]) = println(combinations(3, List("a", "b", "c", "d")))
//  def flatMapSublists[A,B](ls: List[A])(f: (List[A]) => List[B]): List[B] =
//    ls match {
//      case Nil => Nil
//      case sublist@(_ :: tail) => f(sublist) ::: flatMapSublists(tail)(f)
//    }
//
//  def combinations[A](n: Int, ls: List[A]): List[List[A]] =
//    if (n == 0) List(Nil)
//    else flatMapSublists(ls) { sl =>
//      combinations(n - 1, sl.tail) map {sl.head :: _}
//    }

  def combinations[A](n: Int, ls: List[A]): List[List[A]] =
    def _comb[A](n: Int, acc:List[A], ls:List[A]): List[List[A]] =
      if ls.isEmpty then acc.reverse::Nil
      else
        _comb(n, ls.head+:acc, ls.tail)::: _comb(n, acc, ls.tail)
    _comb(1, List(), ls).filter(_.length == n)

  def permutation[A](n: Int, ls: List[A]): List[List[A]] =
    def _perm[A](n: Int, acc:List[A], ls:List[A]): List[List[A]] =
      if ls.isEmpty then acc.reverse::Nil
      else
        _perm(n, ls.head+:acc, ls.tail)::: _perm(n, acc, ls.tail)
    _perm(1, List(), ls).filter(_.length == n)
    
    
}