object P19 {
  // List('d, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'a, 'b, 'c)
  def main(args: Array[String]) =
  println(rotate(-2, List('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k')))

  def rotate(slicePart: Int, ls: List[Char]): List[Char] =
    def _rotate(slicePart:Int, ls: List[Char], acc: List[Char]): List[Char]=
      if slicePart>0 && acc.length == slicePart then ls:::acc.reverse
      else if slicePart>0 && acc.length != slicePart then _rotate(slicePart, ls.tail, ls.head::acc)

      else if slicePart<0 && acc.length == -slicePart then acc:::ls.reverse
      else  _rotate(slicePart, acc.head::ls, acc.tail)
    if slicePart > 0 then _rotate(slicePart, ls, Nil)
    else _rotate(slicePart, Nil, ls)

}
