
object P13 {
  def main(args: Array[String]) =  println(encodeDirect(List('a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e')))

  // COPIED FROM stackoverflow, best solution
  // https://stackoverflow.com/questions/14550537/run-length-encoding-using-scala
  def encodeDirect[X](xs: List[X]): List[(Int, X)] = xs match {
    case Nil => Nil
    case y :: ys => encodeDirect(ys) match {
      case (c, `y`) :: rest => (c + 1, y) :: rest
      case             rest => (    1, y) :: rest
    }
  }
}
