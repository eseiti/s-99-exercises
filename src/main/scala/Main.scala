object Main {
  def main(args: Array[String]) = {
    println("Hello, world")
  }

  // The standard functional approach is to recurse down the list until we hit
  // the end.  Scala's pattern matching makes this easy.

  def nth[A](cnt: Int,list: List[A]): A = (cnt, list) match {
    case (0, h:: _) => h
    case (cnt, _:: tail) =>nth(cnt-1, tail)
    case (_, Nil      ) => throw new NoSuchElementException
  }

  def length[A](lst: List[A]): Int = lst match {
    case Nil => 0
    case a:: tail =>  1 + length(tail)
  }


  def isPalindrome[A](lst: List[A]): Boolean = lst.reverse == lst

 
//    def flatten(ls: List[Any]): List[Any] = ls flatMap {
//      case ms: List[_] => flatten(ms)
//      case e => List(e)
//    }


  

}