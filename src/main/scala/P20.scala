object P20 {
  def main(args: Array[String]) = println(removeAt(1, List("a", "b", "c", "d")))

  def removeAt(index: Int, ls: List[String]): (List[String], String) =
    def _removeAt(index:Int, ls:List[String], append:List[String]): (List[String], String) = (index, ls) match {
      case (0,_) => (append.reverse::: ls.tail,ls.head)
      case (_,Nil) => throw new Exception("VK")
      case (ind, list) => _removeAt(ind-1, list.tail, list.head::append)
    }
    _removeAt(index, ls, Nil)

}
