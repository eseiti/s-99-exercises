object P09 {
//  scala> pack(List('a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e'))
//  res0: List[List[Symbol]] = List(List('a, 'a, 'a, 'a), List('b), List('c, 'c), List('a, 'a), List('d), List('e, 'e, 'e, 'e))
  def main(args: Array[String]) = {
    println(pack(List('a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e')))
  }

  def pack(value: List[Char]): List[List[Char]] =
    def _pack(value:List[Char], acc: List[List[Char]]):List[List[Char]] = (value, acc) match
      case(Nil, accc) => accc.reverse
      case(vals, Nil) => _pack(vals.tail, List(List(vals.head)))
      case(el::tail, el1::tail1) => if el == el1.head then _pack(tail, (el::el1)::tail1)
                                    else _pack(tail, List(el) +: el1 +: tail1)




      _pack(value, Nil)

}
