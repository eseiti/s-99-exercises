
object P08 {
  def main(args: Array[String]) = println(compress(List('a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e')))

 def compress(ls: List[Char]): List[Char] =
   def compressList(ls:List[Char], acc:List[Char]):List[Char] = (ls,acc) match
     case (Nil,acc) => acc.reverse
     case (el::tail,Nil) => compressList(tail, el::Nil)
     case (el::tail, accum) => if el == accum.head then compressList(tail,accum)
                                    else compressList(tail,el::accum)

   compressList(ls,Nil)

}
