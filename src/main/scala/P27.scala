object P27 {

  def main(args: Array[String]) = print(
    group(List(2, 2, 5),
    List("Aldo", "Beat", "Carla", "David", "Evi", "Flip", "Gary", "Hugo", "Ida")))
  // 1c, 5c, 10c, 25c, 50c, and $1
//  def getChange(money: Double, productCost: Double): List[Int] =
//    def _getChange(change:Int, coins:List[Int], acc:List[Int]): List[Int] = (change,coins) match {
//      case (0,_) => acc.reverse
//      case (a,el::tail) => val coins:Int = a/ el; _getChange(a-coins*el, tail, coins+:acc)
//    }
//    _getChange(((money-productCost)*100).toInt, List(100,50,25,10,5,1), List())

  def group[A](ns: List[Int], ls: List[A]): List[List[List[A]]] = ns match
    case Nil     => List(Nil)
    case n :: ns => P26.combinations(n, ls) flatMap { c =>
      group(ns, ls diff c) map {c :: _}
    }

}
