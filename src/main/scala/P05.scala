import P03.nth

object P05 {
  def main(args: Array[String]) = {
    println(reverse(List(1, 1, 2, 3, 5, 8)))
  }

  def reverse(lst: List[Int]):List[Int] =
    def rev(lst:List[Int], acc:List[Int]): List[Int] = lst match
      case el::tail => rev(tail, el+:acc)
      case Nil => acc

    rev(lst,Nil)

}
