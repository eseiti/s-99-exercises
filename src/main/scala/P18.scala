object P18 {
  def main(args:Array[String]) = println( slice(3, 7, List('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k')))

  def slice(start: Int, end: Int, ls: List[Char]): List[Char] =
    def _slice(index:Int, start: Int, end: Int, ls: List[Char], acc: List[Char]): List[Char] = (index,ls) match
      case (_, Nil) => throw new NoSuchElementException("The slicing could not be done")
      case (i,el::tail) => if i>= end then acc.reverse
                           else if i>=start && i<end then _slice(i+1, start, end, tail, el::acc)
                          else _slice(i+1, start, end, tail, acc)

    _slice(0, start, end, ls, Nil)
}
