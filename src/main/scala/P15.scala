object P15 {

  def main(args: Array[String]) = println(duplicateN(3, List('a', 'b', 'c', 'c', 'd')))

  //def duplicateN(n:Int, value:List[Char]) :List[Char] = value.flatMap(e=> List(e,e))

  def duplicateN(n:Int, value: List[Char]): List[Char] = value match {
    case Nil => Nil
    case el::tail => List.fill(n)(el)::: duplicateN(n, tail)
  }
}
