import P27.group

object P28 {

  def main(args: Array[String]) = print(
  lsort(List(List("a", "b", "c")
    , List("d", "e"), List("f", "g", "h"), List("d", "e"), List("i", "j", "k", "l"), List("m", "n"), List("o"))))

  def lsort[A](ls: List[List[A]]): List[List[A]] =
    def _lsort[A](ls: List[List[A]], acc: List[List[A]]): List[List[A]] = (ls,acc) match
        case (Nil,_) => acc
        case (_, Nil) => _lsort(ls.tail, ls.head::Nil)
        case (el::tail,_) => if el.length <= acc.head.length then _lsort(tail, el::acc)
                             else _lsort(tail, insert(el, acc, List()))

    _lsort(ls, List())

  def insert[A](el:List[A], ls: List[List[A]], left: List[List[A]]): List[List[A]] = ls match {
    case Nil => left.reverse:::el::Nil
    case head::tail => if el.length > head.length then insert(el, tail, head::left)
    else left.reverse:::el::tail
  }



}
